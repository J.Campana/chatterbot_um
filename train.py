# -*- coding: utf-8 -*-
from os import listdir
from os.path import isfile, join
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer

onlyfiles = [f for f in listdir("./data") if isfile(join("./data", f))]

chat = ChatBot(  # se declara la entidad del chat, nueva instancia del bot
    'chatterbot',  # nombre del bot
    storage_adapter='chatterbot.storage.SQLStorageAdapter',  # adaptador para el almacenamiento en SQLite
    # database='./data/database.sqlite',                       # ubicacion del archivo SQLite donde se guardan los datos
    trainer='chatterbot.trainers.ListTrainer',  # adaptador logico para el entrenamiento
    # logic_adapters=[{                                        # clase que devuelve respuesta
    # "import_path": "chatterbot.logic.BestMatch",
    # "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
    # "response_selection_method": "chatterbot.response_selection.get_most_frequent_response"
    # }]
)

trainer = ListTrainer(chat)
initialtrainer = ChatterBotCorpusTrainer(chat)

for file in onlyfiles:
    initialtrainer.train("./data/" + file)