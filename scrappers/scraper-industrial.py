from bs4 import BeautifulSoup
import requests
import unidecode

URL = "http://um.edu.ar/es/fi/carreras/ingenieria-industrial.html"

questions = []
responses = []

# Realizamos la petición a la web
req = requests.get(URL)

# Comprobamos que la petición nos devuelve un Status Code = 200
status_code = req.status_code

# Si trae los datos del html realiza las tareas del scrapper
if status_code == 200:

    # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
    html = BeautifulSoup(req.text, "html.parser")
    # Obtenemos todos los divs donde están las entradas
    entradas = html.find_all('div', {'class': 'contenido'})
    # Obtenemos todos los parrafos
    content = html.find_all('p')
    # Obtenemos todos los header de h2
    titles1 = html.find_all('h2')
    # Obtenemos todos los header de h3
    titles2 = html.find_all('h3')
    # Se itera para cada parrafo

    for p in content:

        # Por que estudiar ingenieria industrial
        if "Porque el ingeniero industrial es un profesional" in p.prettify().lower() \
                or "porque el ingeniero industrial es un profesional" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            # formatted_text = unidecode.unidecode(text[text.index("La carrera de"):].replace(':', ''))
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("por que estudiar ingenieria industrial")
            responses.append(formatted_text)
            questions.append("razones para ser ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial razones")
            responses.append(formatted_text)

        #Perfil del ingeniero industrial
        if "La carrera de" in p.prettify().lower() or "la carrera de" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            #formatted_text = unidecode.unidecode(text[text.index("La carrera de"):].replace(':', ''))
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("perfil del ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial perfil")
            responses.append(formatted_text)
            questions.append("cual es el perfil de un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("a que se orienta un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial a que se orienta")
            responses.append(formatted_text)

        #Formacion de materias
        if "Este profesional" in p.prettify().lower() or "este profesional" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()

            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()

            # Se añade la pregunta a la cual se debe responder
            questions.append("formacion ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial formacion")
            responses.append(formatted_text)
            questions.append("en que se forma un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("bases de un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial materias importantes")
            responses.append(formatted_text)

        # En que trabaja un ingeniero industrial
        if "Se ha concebido" in p.prettify().lower() or "se ha concebido" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("donde trabaja un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("en que trabaja un ingeniero industrial")
            responses.append(formatted_text)
            questions.append("trabajo ingeniero industrial")
            responses.append(formatted_text)

            # Alcances del titulo
        if "definidos por la resolución" in p.prettify().lower() or "definidos por la resolución" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero industrial")
            responses.append(formatted_text)
            questions.append("ingeniero industrial alcances del titulo")
            responses.append(formatted_text)

        # Por quien está aprobada
        if "Validez nacional" in p.prettify().lower() or \
                "validez nacional" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("ingenieria industrial por quien esta aprobada")
            responses.append(formatted_text)
            questions.append("por quien esta aprobada ingenieria industrial")
            responses.append(formatted_text)

        # Se abre el archivo en modo escritura
    file = open("./data/responses-ind.yml", "w+")
    # Se escriben las primeras lineas del yaml necesarias para el ChatBot
    file.write("categories: \n- general \nconversations: \n")

            # Para cada pregunta se asocia una respuesta
    for i in range(len(responses)):
        file.write("- - " + questions[i] + "\n")
        file.write("  - " + responses[i] + "\n")

# En caso de que la pagina no devuelva un codigo 200 (Exito) se va a el error
else:
    print("Status Code %d" % status_code)
