from bs4 import BeautifulSoup
import requests
import unidecode

URL = "http://um.edu.ar/es/fi/carreras/ingenieria-en-computacion.html"

questions = []
responses = []

# Realizamos la petición a la web
req = requests.get(URL)

# Comprobamos que la petición nos devuelve un Status Code = 200
status_code = req.status_code

# Si trae los datos del html realiza las tareas del scrapper
if status_code == 200:
    # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
    html = BeautifulSoup(req.text, "html.parser")
    # Obtenemos todos los divs donde están las entradas
    entradas = html.find_all('div', {'class': 'contenido'})
    # Obtenemos todos los parrafos
    content = html.find_all('p')
    # Obtenemos todos los header de h2
    titles1 = html.find_all('h2')
    # Obtenemos todos los header de h3
    titles2 = html.find_all('h3')
    # Se itera para cada parrafo

    for p in content:
        # Si se encuentra la palabra duracion en el parrafo se realiza lo de abajo
        if "duracion" in p.prettify().lower() or "duración" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text[text.index("Duración"):].replace(':', ''))
            # Se añade la pregunta a la cual se debe responder
            questions.append("duracion computacion")
            responses.append(formatted_text)
            questions.append("dura computacion")
            responses.append(formatted_text)
            questions.append("Cuanto dura ingenieria?")
            responses.append(formatted_text)
            questions.append("Cuanto dura ingenieria en computacion?")
            responses.append(formatted_text)
            questions.append("Cuantos anos dura la carrera ingenieria en computacion?")
            responses.append(formatted_text)
            questions.append("computacion duracion")
            responses.append(formatted_text)

        # Por que estudiar ingenieria en computacion
        if "Para que no seas solo usuario" in p.prettify().lower() \
                or "para que no seas solo usuario" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # formatted_text = unidecode.unidecode(text[text.index("La carrera de"):].replace(':', ''))
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()   
            # Se añade la pregunta a la cual se debe responder
            questions.append("por que estudiar ingenieria en computacion")
            responses.append(formatted_text)
            questions.append("por que ser un ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("razones para ser ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion razones")
            responses.append(formatted_text)
            questions.append("computacion razones")
            responses.append(formatted_text)
            questions.append("razones computacion")
            responses.append(formatted_text)

        if "Porque el sector productivo" in p.prettify().lower() \
                or "porque el sector productivo" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # formatted_text = unidecode.unidecode(text[text.index("La carrera de"):].replace(':', ''))
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()      
            # Se añade la pregunta a la cual se debe responder
            questions.append("por que es necesario el ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("necesidad de ingenieros en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion necesidad")
            responses.append(formatted_text)
            questions.append("computacion necesidad")
            responses.append(formatted_text)
            questions.append("necesidad computacion")
            responses.append(formatted_text)

        # Perfil del ingeniero en computación
        if "La currícula de" in p.prettify().lower() or "la currícula de" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            #formatted_text = unidecode.unidecode(text[text.index("La carrera de"):].replace(':', ''))
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()        
            # Se añade la pregunta a la cual se debe responder
            questions.append("perfil del ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion perfil")
            responses.append(formatted_text)
            questions.append("cual es el perfil de un ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("a que se orienta un ingeniero computacion")
            responses.append(formatted_text)
            questions.append("orientacion ingeniero computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion a que se orienta")
            responses.append(formatted_text)
            questions.append("que puede hacer un ingeniero en computacion?")
            responses.append(formatted_text)

        # Formacion del egresado
        if "Tiene una completa" in p.prettify().lower() or "tiene una completa" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("formacion ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion formacion")
            responses.append(formatted_text)
            questions.append("en que se forma un ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("bases de un ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("bases ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("bases computacion")
            responses.append(formatted_text)
            questions.append("computacion bases")
            responses.append(formatted_text)

        # Capacitacion un ingeniero en computacion
        if "Su capacitación" in p.prettify().lower() or "su capacitación" in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("capacitacion ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("en que esta capacitado un ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion capacitacion")
            responses.append(formatted_text)

        # Alcances del titulo
        if '2.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('2.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '3.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('3.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '4.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('4.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '5.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('5.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '6.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('6.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '7.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('7.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '8.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('8.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Alcances del titulo
        if '9.' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace('9.', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("alcances del titulo ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion alcances del titulo")
            responses.append(formatted_text)
            questions.append("alcances computacion")
            responses.append(formatted_text)
            questions.append("computacion alcances")
            responses.append(formatted_text)
            questions.append("alcance computacion")
            responses.append(formatted_text)
            questions.append("computacion alcance")
            responses.append(formatted_text)

        # Competencias del ingeniero en computacion
        if 'Las competencias' in p.prettify().lower() or 'las competencias' in p.prettify().lower():
            # Se obtiene el texto limpio sin tags
            text = p.get_text()
            # Se elimina lo anterior a duracion, se reemplazan los dos puntos y caracteres especiales.
            formatted_text = unidecode.unidecode(text.replace(':', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\t', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\r', ''))
            formatted_text = unidecode.unidecode(formatted_text.replace('\n', ''))
            formatted_text = formatted_text.strip()
            # Se añade la pregunta a la cual se debe responder
            questions.append("competencias del ingeniero en computacion")
            responses.append(formatted_text)
            questions.append("ingeniero en computacion competencias")
            responses.append(formatted_text)
            questions.append("competencias computacion")
            responses.append(formatted_text)
            questions.append("computacion competencias")
            responses.append(formatted_text)

# Se abre el archivo en modo escritura
    file = open("./data/computacion.yml", "w+")
    # Se escriben las primeras lineas del yaml necesarias para el ChatBot
    file.write("categories: \n- general \nconversations: \n")

    # Para cada pregunta se asocia una respuesta
    for i in range(len(responses)):
        file.write("- - " + questions[i] + "\n")
        file.write("  - " + responses[i] + "\n")

    print("Scraper de Computación terminado con exito")

# En caso de que la pagina no devuelva un codigo 200 (Exito) se va a el error
else:
    print("Status Code %d" % status_code)
